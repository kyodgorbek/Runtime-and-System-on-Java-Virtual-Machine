# Runtime-and-System-on-Java-Virtual-Machine
Runtime and System on Java Virtual Machine
public class Runtime
	extends java.langObject Basic-J2se-Math-Class{
	// Static methods
	public static Runtime getRuntime();

	// Methods
	public void exit(int status);
	public long freememory();
	public void gc();
	public  long totalmemory();
}

